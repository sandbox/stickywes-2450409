<?php


$config['sandbox']=TRUE;
$config['storeAccessTokenToSession']=TRUE;
$config['client_secret'] = '4205DQXBAP99S8SUHXI3';
$config['auth_redirect'] = 'http://localhost/';
$config['client_id'] = 'sandbox';
$config['user_tag_field'] = 'field_field_feedly_tags';
$config['user_access_key_field'] = 'field_field_feedly_access_key';
$config['user_feedly_refresh_token_field'] = 'field_feedly_refresh_token';
$config['user_access_expires_field'] = 'field_feedly_access_expires';
$config['auth_callback'] = 'http://localhost/';
$config['auth_access_token'] = '';

$config['entity_type'] = 'node';
$config['entity_bundle'] = 'feedly_entry';
// Text
$config['entry_id_field'] = 'field_id';
// Text
$config['entry_title_field'] = 'title';
// Should be a field with a summary text and full text.
// LONG text.
$config['entry_content_field'] = 'body';
// Probably not going to be used.
$config['entry_summary_field'] = 'body';
// Text.
$config['entry_author_field'] = 'field_author';
// Date Timestamp.
$config['entry_crawled_field'] = 'field_crawled';
// Date Timestamp.
$config['entry_recrawled_field'] = 'field_recrawled';
// Date Timestamp.
$config['entry_published_field'] = 'field_published';
// Date Timestamp.
$config['entry_updated_field'] = 'field_updated';
// Infinite Long Text Array.  Should be more complex, will be serialized JSON for now.
$config['entry_alternate_field'] = 'field_altnerate';
// Long Text field.  Should be more complex, will be serialized JSON for now.
$config['entry_origin_field'] = 'field_entry_origin';
// Infinite Text field.
$config['entry_keywords_field'] = 'field_keywords_field';
// Text field.  Should be more complex, will be serialized JSON for now.
$config['entry_visual_field'] = 'field_visual_field';
// Boolean field.
$config['entry_unread_field'] = 'field_unread_fied';
// Infinite text field.  Could've been JSON blobs but we can get labels from tags API.
$config['entry_tags_ids_field'] = 'field_tags_ids_field';
// Infinite text field.  Could've been JSON blobs but we can get labels from categories API.
$config['entry_categories_ids_field'] = 'field_categories_ids_field';
// Integer field.
$config['entry_engagement_field'] = 'field_engagement_field';
// Date timestamp.
$config['entry_action_timestamp_field'] = 'field_action_timestamp';
// Infinite text.  Ignoring anything other than URLs.
$config['entry_enclosure_field'] = 'field_enclosure';
// Text field.
$config['entry_fingerprint_field'] = 'field_fingerprint';
// Text field.
$config['entry_origin_id_field'] = 'field_origin_id';
// Text field.
$config['entry_sid_field'] = 'field_sid';
